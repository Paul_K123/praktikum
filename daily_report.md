**22.06.2020**



**Target-Protein**



**ASXL1 - Polycomb group protein ASXL1**
robable Polycomb group (PcG) protein involved in transcriptional regulation mediated by ligand-bound nuclear hormone receptors, such as retinoic acid receptors (RARs) and peroxisome proliferator-activated receptor gamma (PPARG) (PubMed:16606617). Acts as coactivator of RARA and RXRA through association with NCOA1 (PubMed:16606617). Acts as corepressor for PPARG and suppresses its adipocyte differentiation-inducing activity (By similarity). Non-catalytic component of the PR-DUB complex, a complex that specifically mediates deubiquitination of histone H2A monoubiquitinated at 'Lys-119' (H2AK119ub1) (PubMed:20436459). Acts as a sensor of N6-methyladenosine methylation on DNA (m6A): recognizes and binds m6A DNA, leading to its ubiquitination and degradation by TRIP12, thereby inactivating the PR-DUB complex and regulating Polycomb silencing 

**ASXL2 - ASXL transcriptional regulator 2**

Putative Polycomb group (PcG) protein. PcG proteins act by forming multiprotein complexes, which are required to maintain the transcriptionally repressive state of homeotic genes throughout development. PcG proteins are not required to initiate repression, but to maintain it during later stages of development. They probably act via methylation of histones, rendering chromatin heritably changed in its expressibility (By similarity). Involved in transcriptional regulation mediated by ligand-bound nuclear hormone receptors, such as peroxisome proliferator-activated receptor gamma (PPARG). Acts as coactivator for PPARG and enhances its adipocyte differentiation-inducing activity; the function seems to involve differential recruitment of acetylated and methylated histone H3.

**MBD5 - methyl-CpG binding domain protein 5**

This gene encodes a member of the methyl-CpG-binding domain (MBD) family. The MBD consists of about 70 residues and is the minimal region required for a methyl-CpG-binding protein binding specifically to methylated DNA. In addition to the MBD domain, this protein contains a PWWP domain (Pro-Trp-Trp-Pro motif), which consists of 100-150 amino acids and is found in numerous proteins that are involved in cell division, growth and differentiation. Mutations in this gene cause an autosomal dominant type of cognitive disability. The encoded protein interacts with the polycomb repressive complex PR-DUB which catalyzes the deubiquitination of a lysine residue of histone 2A. Haploinsufficiency of this gene is associated with a syndrome involving microcephaly, intellectual disabilities, severe speech impairment, and seizures. Alternatively spliced transcript variants have been found, but their full-length nature is not determined. [provided by RefSeq, Jul 2017]
Lineage: Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi; Mammalia; Eutheria; Euarchontoglires; Primates; Haplorrhini; Catarrhini; Hominidae; Homo
Expression: Ubiquitous expression in brain (RPKM 2.6), gall bladder (RPKM 2.4) and 25 other tissues
Domans: CpG binding domain

**MBD6 - methyl-CpG binding domain protein 6**

Lineage: Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi; Mammalia; Eutheria; Euarchontoglires; Primates; Haplorrhini; Catarrhini; Hominidae; Homo
Expression: Ubiquitous expression in testis (RPKM 15.8), spleen (RPKM 15.7) and 25 other tissues
KDM1B lysine demethylase 1B

Summary: Flavin-dependent histone demethylases, such as KDM1B, regulate histone lysine methylation, an epigenetic mark that regulates gene expression and chromatin function (Karytinos et al., 2009 [PubMed 19407342]).[supplied by OMIM, Oct 2009]
Lineage: Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi; Mammalia; Eutheria; Euarchontoglires; Primates; Haplorrhini; Catarrhini; Hominidae; Homo
Expression: Broad expression in thyroid (RPKM 17.9), testis (RPKM 11.6) and 23 other tissues

**KDM2B - lysine demethylase 2B**

Lineage: Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi; Mammalia; Eutheria; Euarchontoglires; Primates; Haplorrhini; Catarrhini; Hominidae; Homo
Also known as: CXXC2; Fbl10; PCCX2; FBXL10; JHDM1B
Summary: This gene encodes a member of the F-box protein family which is characterized by an approximately 40 amino acid motif, the F-box. The F-box proteins constitute one of the four subunits of ubiquitin protein ligase complex called SCFs (SKP1-cullin-F-box), which function in phosphorylation-dependent ubiquitination. The F-box proteins are divided into 3 classes: Fbws containing WD-40 domains, Fbls containing leucine-rich repeats, and Fbxs containing either different protein-protein interaction modules or no recognizable motifs. The protein encoded by this gene belongs to the Fbls class. Multiple alternatively spliced transcript variants have been found for this gene, but the full-length nature of some variants has not been determined. [provided by RefSeq, Jul 2008]
Expression: Ubiquitous expression in lymph node (RPKM 6.6), appendix (RPKM 5.9) and 25 other tissues


**Table 1 Quelle: Schuettengruber et al. 2017**
PcG Complex Components 	    Characteristic Domain	    (Epigenetic) Function	            Zugehörige Komplexe
ASXL1/2	                    N/A 	                    chromatin bindinding	            Core PR-DUB (TrxG)
MBD5/6	                    methyl binding 	            DNA binding	                        PR-DUB Accessory Proteins (PcG)
KDM1B                   	amine oxidase 	            histone demethylation	            PR-DUB Accessory Proteins
KDM2B	                    JmjC, CxxC 	                H3K36 demethylase, DNA binding      Non-canonical PRC1 (PcG)


Noch zum Upload bereicht:
- Fasta-Datein von allen 6 Proteinen
- Bilddatei des Trees mit Spezieszuordnung
- AligmentFasta-Datei von ASXH Domane 


**ASXH Domane:**
A conserved alpha helical domain with a characteristic LXXLL motif. 
The LXXLL motif is detected in diverse transcription factors, coactivators and corepressors and is implicated in mediating interactions between them. 
The ASXH domain is found in animals, fungi and plants and is predicted to play a role in mediating contact between transcription factors and 
chromatin-associated complexes. In Drosophila Asx and Human ASXL1, the ASXH domain is predicted to mediate interactions with the Calypso and BAP1 deubiquitinases (DUBs) 
which further belong to the UCHL5/UCH37 clade of DUBs.  Fasta (ASXH Domane)
(https://pfam.xfam.org/family/PF13919)

**Sonstiges**

- proeteomTable Link berichtigt (S. helinthica)
 

- Hinweiß aus Chats : 
- Ichthyophonus hoferi scheint zu Ichthyosporea zu gehören. https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=39843
- @Björn: hier ist eine relativ einfache Einleitung in blastall: http://wiki.christophchamp.com/index.php?title=Blastall Es gibt natürlich andere. Die kXX.bioinf.uni-leipzig.de haben auch die neuere BLAST+ Software, die im Grunde blastall äquivalent ist. Dafür gibt's auch Tutorials, z.B. https://angus.readthedocs.io/en/2016/running-command-line-blast.html
- blastall ist auf die kXX.bioinf.uni-leipzig.de Rechner installiert.


**23.06.2020**

Datenbankerstellung:
- kein Genomlink für TSPH	Trichoplax sp. H2 --> Spezies: Placozoa
- kein Proteomlink für HHON	Hoilungia hongkongensis	bitbucket--> Spezies: Placozoa
- kein Genomlink für SCAR	Stylissa carteri, XTES	Xestospongia testudinaria, EMUE	Ephydatia muelleri--> Spezies: Porifera
- kein Proteomlink für PBAC	Pleurobrachia bachei--> Spezies: Ctenophora
- kein Genomlink für BFOR	Beroe forskalii, BOVA	Beroe ovata	--> Spezies: Ctenophora
- kein Proteomlink für CFRA	Creolimax fragrantissim --> Spezies: Teretosporea
- kein Proteomlink für IHOF	Ichthyophonus hoferi --> Spezies: Teretosporea
- kein Proteomlink für AWHI	Abeoforma whisleri --> Spezies: Teretosporea
- kein Proteomlink für CPER	Chromosphaera perkinsi --> Spezies: Teretosporea
- kein Proteomlink für CLIM	Corallochytrium limacisporum --> Spezies: Teretosporea

Not working: 
LEI	Mnemiopsis leidyi	EnsemblGenomes	ftp.ensemblgenomes.org/pub/release-47/metazoa/fasta/mnemiopsis_leidyi/pep/Mnemiopsis_leidyi.MneLei_Aug2011.pep.all.fa.gz	ftp.ensemblgenomes.org/pub/release-47/metazoa/fasta/mnemiopsis_leidyi/dna/Mnemiopsis_leidyi.MneLei_Aug2011.dna.toplevel.fa.gz	---



No Data:

MVIB	Ministeria vibrans
APAR	Amoebidium parasiticum
DES 	Sphaerothecum destruens


Blast

> makeblastdb -in proteom_input -out proteom_name -parse_seqids -dbtype prot  
> blastp -query ASXL1.fa -db proteom -out output.txt




Einrichtung des Blast-Tools

Problem (seit Praktikumsbeginn):
1. Klonierung von GitLab bzw Zugriff auf die Unirechner (alles via SSH-Schlüssel) -> Antje
2. kein pushen auf GitLab möglich -> Nora

Windows als mögliche Problemquelle?


**24.06.2020**

Blast-Ergbenisse:

- SCAR funktionierte noch nicht als db zu generieren--> Fehlermeldung: BLAST Database creation error: Near line 17, the local id is too long. Its length is 58 but the maximum allowed local id length is 50. Please find and correct all local ids that are too long
- SDIP Proteom gibt es zweimal (1mal france) --> unterschiede noch nicht ganz klar jedoch andere matches
- SDIP:  36,167 sequences; 11,673,123 total letters
- SDIP "france":  43,380 sequences; 12,880,019 total letters

Auffälligkeiten/Fragen:

ATHA -> keine der Proteine, aber methyl-CPG-binding domain protein 02
AXL! --> hohe E-values mit Insulin-degrading enzyme ---> Haben die ertwas mit einander zutn?
Hohe E-Values zeigen niedrigte Identitätsraten?..wie lassen sich die Erebnisse richtig zuordnen?
--> annotierte Proteine (z.B. SCIL_KDMAB) weißt auch Identitätsraten von mur 46% auf

**Vergleich Blast-Ergebnisse mit Schuettengruber et al.**

ALs ersten Ansatz habe ich geschaut, bei welchen Proteinen konnten eindeutig das Protein gemacht werden. D.H hoihe E-Values und keine andere notation im Proteom.
Bei konnten wengiger Proteine nachgewiesen werden als bei Schuettengruber. --> zu Beachten: bei uns wurde lediglich nur eine Proteinseq auf das Proteom geblastet. 
Es gab auch annnotierte Sequenzen die bis jetzt erstmal unbeachtet blieben. 

NEU(25.06.2020: Ergbenisse berichtigt, stimmen nun mit Schuettengruber überein!!!!!


--> Alle Blast-Ergebnisse hochgeladen! (Ergebnisse.zip) + Visualisierung (Ergebnis_visual_Evalues.pdf)

**25.06.2020**

**Domänen (InterPro)**



*ASXL1/2*

Protein ASX-like, PHD domain

Description
This entry represetnts the PHD domain, a DNA-binding domain, on the additional sex combs (ASX) and additional sex combs-like (AsxL) proteins. The Asx protein acts as an enhancer of trithorax and polycomb in displaying bidirectional homoeotic phenotypes in Drosophila, suggesting that it is required for maintenance of both activation and silencing of Hox genes. Asx is required for normal adult haematopoiesis and its function depends on its cellular context 

Molecular Function : dna binding 

ASXL, HARE-HTH domain

Description
The Asx-like (Asxl) proteins includes Asxl1-3. They are putative Polycomb group (PcG) proteins, which act by forming multiprotein complexes that are required to maintain the transcriptionally repressive state of homeotic genes throughout development. Asxl1 is involved in transcriptional regulation mediated by ligand-bound retinoic acid receptors (RARs) and peroxisome proliferator-activated receptor gamma (PPARG).
The delta protein is a dispensable subunit of Bacillus subtilis RNA polymerase (RNAP) that has major effects on the biochemical properties of the purified enzyme. In the presence of delta, RNAP displays an increased specificity of transcription, a decreased affinity for nucleic acids, and an increased efficiency of RNA synthesis because of enhanced recycling. The delta protein, contains two distinct regions, an N-terminal domain and a glutamate and aspartate residue-rich C-terminal region. It participates in both the initiation and recycling phases of transcription.
This domain, known as the HARE-HTH domain, adopts the winged helix-turn-helix fold and is predicted to bind DNA. It can be found at the N terminus of the ASXL protein. It can also be found in several other eukaryotic chromatin proteins (such as HB1 in plants), diverse restriction endonucleases and DNA glycosylases, the RNA polymerase delta subunit of Gram-positive bacteria and certain bacterial proteins that combine features of the RNA polymerase alpha-subunit and sigma factors. The genetic interaction of the HARE-HTH containing ASXL with the methyl cytosine hydroxylating Tet2 protein is suggestive of a role for the domain in discriminating sequences with DNA modifications such as hmC. Bacterial versions include fusions to diverse restriction endonucleases, and a DNA glycosylase where it may play a similar role in detecting modified DNA. Certain bacterial version of the HARE-HTH domain show fusions to the helix-hairpin-helix domain of the RNA polymerase alpha subunit and the HTH domains found in regions 3 and 4 of the sigma factors. These versions are predicted to function as a novel inhibitor of the binding of RNA polymerase to transcription start sites, similar to the Bacillus delta protein 

biological process: 
- transcription, DNA-templated (GO:0006351)
- regulation of transcription, DNA-templated

ASX homology domain

Description
This entry represents a conserved alpha helical domain with a characteristic LXXLL motif found in Asx and homologues. The LXXLL motif is detected in diverse transcription factors, coactivators and co-repressors and is implicated in mediating interactions between them. The Asx homology (ASXH) domain is found in animals, fungi and plants and is predicted to play a role in mediating contact between transcription factors and chromatin-associated complexes. In Drosophila Asx and Human ASXL1, the ASXH domain is predicted to mediate interactions with the Calypso and BAP1 deubiquitinases (DUBs) which further belong to the UCHL5/UCH37 clade of DUBs 

*KDM1B*

Amine oxidase

This entry consists of various amine oxidases, including maize polyamine oxidase (PAO)
[1], L-amino acid oxidases (LAO) and various flavin containing monoamine oxidases (MAO). The aligned region includes the flavin binding site of these enzymes. In vertebrates MAO plays an important role in regulating the intracellular levels of amines via their oxidation; these include various neurotransmitters, neurotoxins and trace amines
[2]. In lower eukaryotes such as aspergillus and in bacteria the main role of amine oxidases is to provide a source of ammonium
[3]. PAOs in plants, bacteria and protozoa oxidise spermidine and spermine to an aminobutyral, diaminopropane and hydrogen peroxide and are involved in the catabolism of polyamines
[1]. Other members of this family include tryptophan 2-monooxygenase, putrescine oxidase, corticosteroid binding proteins and antibacterial glycoproteins.

Zinc finger, CW-type

Zinc finger (Znf) domains are relatively small protein motifs which contain multiple finger-like protrusions that make tandem contacts with their target molecule. Some of these domains bind zinc, but many do not; instead binding other metals such as iron, or no metal at all. For example, some family members form salt bridges to stabilise the finger-like folds. They were first identified as a DNA-binding motif in transcription factor TFIIIA from Xenopus laevis (African clawed frog), however they are now recognised to bind DNA, RNA, protein and/or lipid substrates
[1, 2, 3, 4, 5]. Their binding properties depend on the amino acid sequence of the finger domains and of the linker between fingers, as well as on the higher-order structures and the number of fingers. Znf domains are often found in clusters, where fingers can have different binding specificities. There are many superfamilies of Znf motifs, varying in both sequence and structure. They display considerable versatility in binding modes, even between members of the same class (e.g. some bind DNA, others protein), suggesting that Znf motifs are stable scaffolds that have evolved specialised functions. For example, Znf-containing proteins function in gene transcription, translation, mRNA trafficking, cytoskeleton organisation, epithelial development, cell adhesion, protein folding, chromatin remodelling and zinc sensing, to name but a few
[6]. Zinc-binding motifs are stable structures, and they rarely undergo conformational changes upon binding their target.
This entry represents a CW-type zinc finger motif, named for its conserved cysteine and tryptophan residues. It is predicted to be a highly specialised mononuclear four-cysteine (C4) zinc finger that plays a role in DNA binding and/or promoting protein-protein interactions in complicated eukaryotic processes including chromatin methylation status and early embryonic development. Weak homology to members of
IPR001965 further evidences these predictions. The domain is found exclusively in vertebrates, vertebrate-infecting parasites and higher plants 

SWIRM domain

Description
The SWIRM domain is a small alpha-helical domain of about 85 amino acid residues found in eukaryotic chromosomal proteins. It is named after the proteins SWI3, RSC8 and MOIRA in which it was first recognised. This domain is predicted to mediate protein-protein interactions in the assembly of chromatin-protein complexes. The SWIRM domain can be linked to different domains, such as the ZZ-type zinc finger (IPR000433), the Myb DNA-binding domain (IPR001005), the HORMA domain (IPR003511), the amino-oxidase domain, the chromo domain (IPR000953), and the JAB1/PAD1 domain.
Molecular Function : protein binding 

 


*KDM2B*

Zinc finger, PHD-type

Description
Zinc finger (Znf) domains are relatively small protein motifs which contain multiple finger-like protrusions that make tandem contacts with their target molecule. Some of these domains bind zinc, but many do not; instead binding other metals such as iron, or no metal at all. For example, some family members form salt bridges to stabilise the finger-like folds. They were first identified as a DNA-binding motif in transcription factor TFIIIA from Xenopus laevis (African clawed frog), however they are now recognised to bind DNA, RNA, protein and/or lipid substrates. Their binding properties depend on the amino acid sequence of the finger domains and of the linker between fingers, as well as on the higher-order structures and the number of fingers. Znf domains are often found in clusters, where fingers can have different binding specificities. There are many superfamilies of Znf motifs, varying in both sequence and structure. They display considerable versatility in binding modes, even between members of the same class (e.g. some bind DNA, others protein), suggesting that Znf motifs are stable scaffolds that have evolved specialised functions. For example, Znf-containing proteins function in gene transcription, translation, mRNA trafficking, cytoskeleton organisation, epithelial development, cell adhesion, protein folding, chromatin remodelling and zinc sensing, to name but a few. Zinc-binding motifs are stable structures, and they rarely undergo conformational changes upon binding their target. This entry represents the PHD (homeodomain) zinc finger domain, which is a C4HC3 zinc-finger-like motif found in nuclear proteins thought to be involved in chromatin-mediated transcriptional regulation. The PHD finger motif is reminiscent of, but distinct from the C3HC4 type RING finger.
The function of this domain is not yet known but in analogy with the LIM domain it could be involved in protein-protein interaction and be important for the assembly or activity of multicomponent complexes involved in transcriptional activation or repression. Alternatively, the interactions could be intra-molecular and be important in maintaining the structural integrity of the protein. In similarity to the RING finger and the LIM domain, the PHD finger is thought to bind two zinc ions.

F-box domain

Description
First identified in cyclin-F as a protein-protein interaction motif, the F-box is a conserved domain that is present in numerous proteins with a bipartite structure. Through the F-box, these proteins are linked to the Skp1 protein and the core of SCFs (Skp1-cullin-F-box protein ligase) complexes. SCFs complexes constitute a new class of E3 ligases. They function in combination with the E2 enzyme Cdc34 to ubiquitinate G1 cyclins, Cdk inhibitors and many other proteins, to mark them for degradation. The binding of the specific substrates by SCFs complexes is mediated by divergent protein-protein interaction motifs present in F-box proteins, like WD40 repeats, leucine rich repeats or ANK repeats.
Molecular Function : protein binding

*MBD5/6*

Methyl-CpG DNA binding

Description
Methylation at CpG dinucleotide, the most common DNA modification in eukaryotes, has been correlated with gene silencing associated with various phenomena such as genomic imprinting, transposon and chromosome X inactivation, differentiation, and cancer. Effects of DNA methylation are mediated through proteins which bind to symmetrically methylated CpGs. Such proteins contain a specific domain of ~70 residues, the methyl-CpG-binding domain (MBD), which is linked to additional domains associated with chromatin, such as the bromodomain, the AT hook motif,the SET domain, or the PHD finger. MBD-containing proteins appear to act as structural proteins, which recruit a variety of histone deacetylase (HDAC) complexes and chromatin remodelling factors, leading to chromatin compaction and, consequently, to transcriptional repression. The MBD of MeCP2, MBD1, MBD2, MBD4 and BAZ2 mediates binding to DNA, in case of MeCP2, MBD1 and MBD2 preferentially to methylated CpG. In case of human MBD3 and SETDB1 the MBD has been shown to mediate protein-protein interactions.
The MBD folds into an alpha/beta sandwich structure comprising a layer of twisted beta sheet, backed by another layer formed by the alpha1 helix and a hairpin loop at the C terminus. These layers are both amphipathic, with the alpha1 helix and the beta sheet lying parallel and the hydrophobic faces tightly packed against each other. The beta sheet is composed of two long inner strands (beta2 and beta3) sandwiched by two shorter outer strands (beta1 and beta4) 
Cellular Component : oxidation-reduction process
Molecular Function : oxidoreductase activity

**"homolog", "paralog" vs. "ortholog"**



homologe Gene, Gene mit ähnlicher Nucleotidsequenz, von denen man deshalb annimmt, daß sie sich von einem gemeinsamen Ur-Gen ableiten. Man unterscheidet 2 Arten der Homologie von Genen: 1) Paraloge Gene (Paraloga) sind homologe Gene in einer Spezies, welche durch Genduplikation (z.B. über inäquales Crossing over) und anschließende Divergenz der Nucleotidsequenzen entstanden sind, wie z.B. die Hox-Gene bei Drosophila oder die verschiedenen Hämoglobin-Gene (Hämoglobine) bei den Säugetieren. 2) Orthologe Gene (Orthologa) dagegen sind homologe Gene in verschiedenen Spezies, die sich von einem gemeinsamen Gen im letzten gemeinsamen Vorfahren der betrachteten Gruppen ableiten, wie z.B. Orthologa des Pax6-Gens bei Drosophila (eyeless) und bei Wirbeltieren (aniridia), oder die orthologen Gene hedgehog bei Drosophila und sonic hedgehog bei den Wirbeltieren. Sequenzhomologie.



**Erledigt:**

-BLAST der Proteine auf die Proteome
-Visualisierung der Balst-Ergebnisse
-Auflistung der Domönen + Funktion
-HMM der Domänen 


**TO DO Liste:**
- Splittree
- Domänen in Proteomen suchen
 
**29.06.2020**

**Splitstree**
Schreiben eines Shell Scripts zur Erzeugung von MSAs mit Clustalo. MSAs sollen in Splitstree eingegeben werden.

**HMMER**
Wir haben heute die Auswertung mit HMMER weitergeführt. Da wir viele Spezies hatten, bei denen uns eine zusätzliche Suche sinnvoll erschien, sind wir nun dabei, eine halbwegs automatisierte Variante der Auswertung zu entwickeln. Wir haben mit dem --domtblout Befehl in hmmsearch Tabellen mit den 25 besten Hits per Spezies und Protein erstellt. Heute haben wir weitere Skripte geschrieben, um die Datenmenge zu reduzieren, um eine Auwertung zu vereinfachen.

**To Do Liste**
Splitstree

**30.6.2020**

**Splitstree**

Aufgaben des Shell Scripts:
1. Download der Proteome mittels wget-Funktion
2. Entpacken der zip-Dateien
3. Anlegen einer Datenbank für Blast
4. Mittels for-Schleife werden alle Protein-Seq gegen die Datenbank geblastet & Alignments erstellt. Es sollen nur die besten Hits ausgegeben werden, gefiltert wird über den E-Value. Anschließend werden die Fasta-Seq der Matche-Seq aus der Datenbank gefiltert.
5. Erstellen multipler Alignments mittels Clustal.

Aus unbekannten Gründen erkennt das Script die übergebenen URLs nicht.

**30.6.2020**

Final Report:
-Einleitung --> fertig
-Methoden --> Blastsuche fertig
-Ergebnisse --> Blastsuche fertig

**HMMER**
HMMER hat heute Probleme gemacht, da ich feststellen musste, dass viele meiner Files, die ich mit HMMER erstellt habe, leer waren und ich daher einen Großteil des Tages damit verbracht habe, die Schäden zu beseitigen. Dies ist mir gelungen und ich habe nun Zusammenfassungen aller Messungen, die es mir erlauben, die Ergebnisse grafisch aufzuarbeiten

**To do Liste**
Fehler im Shell Script finden
Splitstree ausführen