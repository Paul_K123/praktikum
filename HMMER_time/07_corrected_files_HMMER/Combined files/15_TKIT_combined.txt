KII73114.1           -            280 F-box                PF00646.34    48     0.016   14.8   0.5   1   2   4.1e-05      0.12   12.0   0.0    10    40    62    92    31    96 0.90 hypothetical protein RF11_03609 [Thelohanellus kitauei]
KII73114.1           -            280 F-box                PF00646.34    48     0.016   14.8   0.5   2   2      0.34     1e+03   -0.5   0.2    11    27   220   236   212   237 0.86 hypothetical protein RF11_03609 [Thelohanellus kitauei]
KII66942.1           -            279 F-box                PF00646.34    48      0.04   13.5   0.4   1   2   4.4e-05      0.13   11.9   0.1     6    44    57    95    46    98 0.93 hypothetical protein RF11_09746 [Thelohanellus kitauei]
KII66942.1           -            279 F-box                PF00646.34    48      0.04   13.5   0.4   2   2         2     6e+03   -3.0   0.0    12    23   220   231   216   235 0.73 hypothetical protein RF11_09746 [Thelohanellus kitauei]
KII62311.1           -            217 F-box                PF00646.34    48     0.077   12.6   0.6   1   1   5.1e-05      0.15   11.7   0.4     7    31   158   182   106   184 0.92 hypothetical protein RF11_15825 [Thelohanellus kitauei]
KII64694.1           -            260 Amino_oxidase        PF01593.25   452     0.045   12.8   0.0   1   1   3.6e-06     0.054   12.5   0.0    16    64    91   147    81   209 0.78 hypothetical protein RF11_05484 [Thelohanellus kitauei]
#
# Program:         hmmsearch
# Version:         3.3 (Nov 2019)
# Pipeline mode:   SEARCH
KII61483.1           -           1213 MBD                  PF01429.20    77     2e-05   23.9   0.0   1   1   4.3e-09   6.4e-05   22.3   0.0    23    74   245   293   224   296 0.79 Nucleosome-remodeling factor subunit [Thelohanellus kitauei]
#
# Program:         hmmsearch
# Version:         3.3 (Nov 2019)
# Pipeline mode:   SEARCH
#
# Program:         hmmsearch
# Version:         3.3 (Nov 2019)
# Pipeline mode:   SEARCH
# Query file:      01_HMMs/03_ASXH.hmm
KII75082.1           -            112 PHD_3                PF13922.7     69      0.16   12.1   3.3   1   1   3.3e-05      0.25   11.5   3.3    46    68     7    29     1    30 0.86 hypothetical protein RF11_03154 [Thelohanellus kitauei]
KII72823.1           -            158 PHD_3                PF13922.7     69       1.5    9.0   5.7   1   2     0.095   7.1e+02    0.4   0.2     5    30    25    52    21    59 0.65 hypothetical protein RF11_14917 [Thelohanellus kitauei]
KII72823.1           -            158 PHD_3                PF13922.7     69       1.5    9.0   5.7   2   2   7.4e-05      0.55   10.4   1.3     6    46    92   131    88   144 0.82 hypothetical protein RF11_14917 [Thelohanellus kitauei]
#
# Program:         hmmsearch
KII72292.1           -            313 PHD_4                PF16866.6     68      0.13   12.1   1.7   1   1   0.00023      0.31   10.9   1.7     5    52   219   264   216   274 0.81 hypothetical protein RF11_14182 [Thelohanellus kitauei]
KII63276.1           -            638 PHD_4                PF16866.6     68      0.15   11.9   2.2   1   2   0.00043      0.59   10.0   1.5    22    43   213   234   202   253 0.80 Retrovirus-related Pol polyprotein [Thelohanellus kitauei]
KII63276.1           -            638 PHD_4                PF16866.6     68      0.15   11.9   2.2   2   2       1.8   2.4e+03   -1.5   0.2     5    18   241   254   237   265 0.69 Retrovirus-related Pol polyprotein [Thelohanellus kitauei]
KII68775.1           -            168 PHD_4                PF16866.6     68      0.17   11.7   3.7   1   2   7.7e-05       0.1   12.4   1.7     5    52    11    56     8    66 0.81 hypothetical protein RF11_12050 [Thelohanellus kitauei]
KII68775.1           -            168 PHD_4                PF16866.6     68      0.17   11.7   3.7   2   2       3.1   4.2e+03   -2.3   0.0    25    39    90   104    78   113 0.65 hypothetical protein RF11_12050 [Thelohanellus kitauei]
KII70441.1           -            128 SKG6                 PF08693.11    38      0.55    9.5   3.7   1   1   6.2e-05      0.93    8.8   3.7    16    36    48    67    47    67 0.81 hypothetical protein RF11_09704 [Thelohanellus kitauei]
#
# Program:         hmmsearch
# Version:         3.3 (Nov 2019)
# Pipeline mode:   SEARCH
KII74217.1           -            369 SWIRM                PF04433.18    90   1.3e-15   57.2   0.4   1   2   6.7e-19   5.1e-15   55.4   0.1    37    90     2    49     1    49 0.98 SWI/SNF complex subunit SMARCC2 [Thelohanellus kitauei]
KII74217.1           -            369 SWIRM                PF04433.18    90   1.3e-15   57.2   0.4   2   2       1.4   1.1e+04   -3.4   0.0    13    24   302   313   290   337 0.63 SWI/SNF complex subunit SMARCC2 [Thelohanellus kitauei]
KII73078.1           -            205 SWIRM                PF04433.18    90     0.034   14.2   0.9   1   1   9.6e-06     0.072   13.2   0.9    22    61    92   130    77   148 0.68 DNA topoisomerase 1 [Thelohanellus kitauei]
#
# Program:         hmmsearch
