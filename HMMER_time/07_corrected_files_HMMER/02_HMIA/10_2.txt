#                                                                            --- full sequence --- -------------- this domain -------------   hmm coord   ali coord   env coord
# target name        accession   tlen query name           accession   qlen   E-value  score  bias   #  of  c-Evalue  i-Evalue  score  bias  from    to  from    to  from    to  acc description of target
#------------------- ---------- ----- -------------------- ---------- ----- --------- ------ ----- --- --- --------- --------- ------ ----- ----- ----- ----- ----- ----- ----- ---- ---------------------
HMIM021111-PA        -            296 zf-CW                PF07496.16    48   5.5e-11   42.6   4.9   1   1   3.4e-14   1.3e-10   41.4   4.9     1    45    17    61    17    62 0.95 pep scaffold:HmiaM1:SCFE01000852.1:19662:120696:-1 gene:HMIM021111 transcript:HMIM021111-RA gene_biotype:protein_coding transcript_biotype:protein_coding
HMIM001598-PA        -            394 zf-CW                PF07496.16    48   6.4e-08   32.8   6.9   1   1   4.5e-11   1.7e-07   31.4   6.9     2    38   122   159   121   164 0.88 pep scaffold:HmiaM1:SCFE01000117.1:203297:211724:1 gene:HMIM001598 transcript:HMIM001598-RA gene_biotype:protein_coding transcript_biotype:protein_coding
HMIM014232-PA        -            572 zf-CW                PF07496.16    48   6.1e-07   29.6   2.0   1   1   6.6e-10   2.5e-06   27.7   2.0     1    46    56    93    56    95 0.83 pep scaffold:HmiaM1:SCFE01000459.1:452032:453750:1 gene:HMIM014232 transcript:HMIM014232-RA gene_biotype:protein_coding transcript_biotype:protein_coding
HMIM001986-PA        -            334 zf-CW                PF07496.16    48   3.5e-05   24.0   0.7   1   1   2.8e-08    0.0001   22.5   0.7     3    47    31    67    30    68 0.87 pep scaffold:HmiaM1:SCFE01000122.1:1321970:1331652:-1 gene:HMIM001986 transcript:HMIM001986-RA gene_biotype:protein_coding transcript_biotype:protein_coding
HMIM007686-PA        -           1381 zf-CW                PF07496.16    48     0.068   13.5   5.4   1   2      0.31   1.2e+03   -0.1   0.0     4    13  1114  1123  1112  1136 0.81 pep scaffold:HmiaM1:SCFE01000248.1:236344:244807:1 gene:HMIM007686 transcript:HMIM007686-RA gene_biotype:protein_coding transcript_biotype:protein_coding
HMIM007686-PA        -           1381 zf-CW                PF07496.16    48     0.068   13.5   5.4   2   2   7.9e-05       0.3   11.4   3.1     2    34  1298  1331  1297  1338 0.80 pep scaffold:HmiaM1:SCFE01000248.1:236344:244807:1 gene:HMIM007686 transcript:HMIM007686-RA gene_biotype:protein_coding transcript_biotype:protein_coding
HMIM014033-PA        -           1456 zf-CW                PF07496.16    48       2.2    8.6  12.8   1   3       1.2   4.3e+03   -1.9   0.1    25    33   223   232   218   236 0.75 pep scaffold:HmiaM1:SCFE01000045.1:1805997:1811312:1 gene:HMIM014033 transcript:HMIM014033-RA gene_biotype:protein_coding transcript_biotype:protein_coding
HMIM014033-PA        -           1456 zf-CW                PF07496.16    48       2.2    8.6  12.8   2   3     0.096   3.6e+02    1.6   0.4     2    11  1091  1100  1090  1100 0.89 pep scaffold:HmiaM1:SCFE01000045.1:1805997:1811312:1 gene:HMIM014033 transcript:HMIM014033-RA gene_biotype:protein_coding transcript_biotype:protein_coding
HMIM014033-PA        -           1456 zf-CW                PF07496.16    48       2.2    8.6  12.8   3   3   0.00014      0.52   10.7   3.3     2    34  1394  1427  1393  1437 0.76 pep scaffold:HmiaM1:SCFE01000045.1:1805997:1811312:1 gene:HMIM014033 transcript:HMIM014033-RA gene_biotype:protein_coding transcript_biotype:protein_coding
#
# Program:         hmmsearch
# Version:         3.3 (Nov 2019)
# Pipeline mode:   SEARCH
# Query file:      01_HMMs/10_zf-CW.hmm
# Target file:     02_Proteoms/02_HMIA_proteom.fa
# Option settings: hmmsearch --domtblout 03_Results_Hmmer/10_2.txt 01_HMMs/10_zf-CW.hmm 02_Proteoms/02_HMIA_proteom.fa 
# Current dir:     /home/paul/Documents
# Date:            Fri Jun 26 13:14:56 2020
# [ok]
