#                                                                            --- full sequence --- -------------- this domain -------------   hmm coord   ali coord   env coord
# target name        accession   tlen query name           accession   qlen   E-value  score  bias   #  of  c-Evalue  i-Evalue  score  bias  from    to  from    to  from    to  acc description of target
#------------------- ---------- ----- -------------------- ---------- ----- --------- ------ ----- --- --- --------- --------- ------ ----- ----- ----- ----- ----- ----- ----- ---- ---------------------
XP_004990408.1       -            525 HARE-HTH             PF05066.14    72   2.4e-07   30.4   0.0   1   1   8.8e-11   5.2e-07   29.3   0.0     5    72   269   332   267   332 0.96 uncharacterized protein PTSG_08618 [Salpingoeca rosetta]
XP_004998277.1       -            157 HARE-HTH             PF05066.14    72       0.1   12.4   0.0   1   2   0.00017      0.99    9.2   0.0    10    36    38    64    33    70 0.86 uncharacterized protein PTSG_00808 [Salpingoeca rosetta]
XP_004998277.1       -            157 HARE-HTH             PF05066.14    72       0.1   12.4   0.0   2   2     0.089   5.2e+02    0.5   0.0    10    29    74    93    69    95 0.85 uncharacterized protein PTSG_00808 [Salpingoeca rosetta]
#
# Program:         hmmsearch
# Version:         3.3 (Nov 2019)
# Pipeline mode:   SEARCH
# Query file:      01_HMMs/05_HARE-HTH.hmm
# Target file:     02_Proteoms/04_SROS_proteom.fa
# Option settings: hmmsearch --domtblout 03_Results_Hmmer/5_4.txt 01_HMMs/05_HARE-HTH.hmm 02_Proteoms/04_SROS_proteom.fa 
# Current dir:     /home/paul/Documents
# Date:            Fri Jun 26 13:21:42 2020
# [ok]
