#                                                                            --- full sequence --- -------------- this domain -------------   hmm coord   ali coord   env coord
# target name        accession   tlen query name           accession   qlen   E-value  score  bias   #  of  c-Evalue  i-Evalue  score  bias  from    to  from    to  from    to  acc description of target
#------------------- ---------- ----- -------------------- ---------- ----- --------- ------ ----- --- --- --------- --------- ------ ----- ----- ----- ----- ----- ----- ----- ---- ---------------------
XP_002117828.1       -            815 zf-CW                PF07496.16    48     0.028   13.8   2.4   1   1   2.3e-05      0.13   11.6   2.4     1    34    94   127    94   137 0.85 hypothetical protein TRIADDRAFT_61865 [Trichoplax adhaerens]
XP_002111444.1       -            122 zf-CW                PF07496.16    48       6.1    6.3  15.0   1   2      0.51   2.9e+03   -2.3   0.7    40    45    51    56    33    59 0.57 hypothetical protein TRIADDRAFT_55483 [Trichoplax adhaerens]
XP_002111444.1       -            122 zf-CW                PF07496.16    48       6.1    6.3  15.0   2   2   2.3e-05      0.13   11.6   7.7     2    33    63    96    62   120 0.73 hypothetical protein TRIADDRAFT_55483 [Trichoplax adhaerens]
#
# Program:         hmmsearch
# Version:         3.3 (Nov 2019)
# Pipeline mode:   SEARCH
# Query file:      01_HMMs/10_zf-CW.hmm
# Target file:     02_Proteoms/18_TADH_proteom.fa
# Option settings: hmmsearch --domtblout 03_Results_Hmmer/10_18.txt 01_HMMs/10_zf-CW.hmm 02_Proteoms/18_TADH_proteom.fa 
# Current dir:     /home/paul/Documents
# Date:            Sun Jun 28 15:23:54 2020
# [ok]
