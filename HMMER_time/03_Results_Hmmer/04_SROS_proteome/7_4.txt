#                                                                            --- full sequence --- -------------- this domain -------------   hmm coord   ali coord   env coord
# target name        accession   tlen query name           accession   qlen   E-value  score  bias   #  of  c-Evalue  i-Evalue  score  bias  from    to  from    to  from    to  acc description of target
#------------------- ---------- ----- -------------------- ---------- ----- --------- ------ ----- --- --- --------- --------- ------ ----- ----- ----- ----- ----- ----- ----- ---- ---------------------
XP_004993679.1       -            391 PHD_4                PF16866.6     68    0.0059   16.1   3.6   1   4     2e-06    0.0059   16.1   3.6    16    67   240   291   229   292 0.73 gluconokinase [Salpingoeca rosetta]
XP_004993679.1       -            391 PHD_4                PF16866.6     68    0.0059   16.1   3.6   2   4      0.72   2.1e+03   -1.7   0.0     6    12   309   315   296   319 0.72 gluconokinase [Salpingoeca rosetta]
XP_004993679.1       -            391 PHD_4                PF16866.6     68    0.0059   16.1   3.6   3   4      0.62   1.8e+03   -1.5   1.5    62    67   336   341   332   342 0.88 gluconokinase [Salpingoeca rosetta]
XP_004993679.1       -            391 PHD_4                PF16866.6     68    0.0059   16.1   3.6   4   4      0.43   1.3e+03   -1.0   0.0     3    18   370   385   369   390 0.80 gluconokinase [Salpingoeca rosetta]
XP_004995891.1       -            866 PHD_4                PF16866.6     68     0.011   15.2   2.8   1   1   1.2e-05     0.036   13.6   2.8    24    67   250   289   234   290 0.88 uncharacterized protein PTSG_03313 [Salpingoeca rosetta]
XP_004994398.1       -           1632 PHD_4                PF16866.6     68       1.4    8.4   5.4   1   1     0.054   1.6e+02    1.9   5.4    25    42  1574  1591  1562  1628 0.75 uncharacterized protein PTSG_04311 [Salpingoeca rosetta]
XP_004992896.1       -            846 PHD_4                PF16866.6     68         7    6.2  12.0   1   3      0.13   3.8e+02    0.7   0.3    29    41     7    19     2    50 0.65 uncharacterized protein PTSG_06004 [Salpingoeca rosetta]
XP_004992896.1       -            846 PHD_4                PF16866.6     68         7    6.2  12.0   2   3       3.3   9.6e+03   -3.8   0.3    63    67    74    78    72    79 0.80 uncharacterized protein PTSG_06004 [Salpingoeca rosetta]
XP_004992896.1       -            846 PHD_4                PF16866.6     68         7    6.2  12.0   3   3   3.9e-05      0.11   12.0   1.1     6    46    84   128    81   144 0.81 uncharacterized protein PTSG_06004 [Salpingoeca rosetta]
#
# Program:         hmmsearch
# Version:         3.3 (Nov 2019)
# Pipeline mode:   SEARCH
# Query file:      01_HMMs/07_PHD_4.hmm
# Target file:     02_Proteoms/04_SROS_proteom.fa
# Option settings: hmmsearch --domtblout 03_Results_Hmmer/7_4.txt 01_HMMs/07_PHD_4.hmm 02_Proteoms/04_SROS_proteom.fa 
# Current dir:     /home/paul/Documents
# Date:            Fri Jun 26 13:22:26 2020
# [ok]
