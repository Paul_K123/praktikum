#                                                                            --- full sequence --- -------------- this domain -------------   hmm coord   ali coord   env coord
# target name        accession   tlen query name           accession   qlen   E-value  score  bias   #  of  c-Evalue  i-Evalue  score  bias  from    to  from    to  from    to  acc description of target
#------------------- ---------- ----- -------------------- ---------- ----- --------- ------ ----- --- --- --------- --------- ------ ----- ----- ----- ----- ----- ----- ----- ---- ---------------------
XP_019856893.1       -            584 PHD_4                PF16866.6     68   4.8e-05   23.8   8.0   1   3   1.6e-08   4.8e-05   23.8   8.0     5    67   196   254   193   255 0.87 PREDICTED: histone acetyltransferase KAT6B-like, partial [Amphimedon queenslandica]
XP_019856893.1       -            584 PHD_4                PF16866.6     68   4.8e-05   23.8   8.0   2   3     0.093   2.8e+02    2.1   2.6    26    67   262   298   256   299 0.75 PREDICTED: histone acetyltransferase KAT6B-like, partial [Amphimedon queenslandica]
XP_019856893.1       -            584 PHD_4                PF16866.6     68   4.8e-05   23.8   8.0   3   3      0.44   1.3e+03   -0.1   0.0    20    41   438   459   430   471 0.74 PREDICTED: histone acetyltransferase KAT6B-like, partial [Amphimedon queenslandica]
XP_003385401.1       -             95 PHD_4                PF16866.6     68    0.0023   18.4   5.8   1   1     6e-06     0.018   15.5   5.8    29    68    41    81     5    81 0.79 PREDICTED: pygopus homolog 2-like [Amphimedon queenslandica]
XP_019851835.1       -            402 PHD_4                PF16866.6     68    0.0037   17.7   4.5   1   3      0.33     1e+03    0.3   0.1     6    17   231   242   194   258 0.67 PREDICTED: zinc finger protein ubi-d4-like isoform X2 [Amphimedon queenslandica]
XP_019851835.1       -            402 PHD_4                PF16866.6     68    0.0037   17.7   4.5   2   3   1.2e-06    0.0037   17.7   4.5     6    67   300   355   295   359 0.79 PREDICTED: zinc finger protein ubi-d4-like isoform X2 [Amphimedon queenslandica]
XP_019851835.1       -            402 PHD_4                PF16866.6     68    0.0037   17.7   4.5   3   3      0.14   4.1e+02    1.6   6.3     7    67   358   402   356   402 0.76 PREDICTED: zinc finger protein ubi-d4-like isoform X2 [Amphimedon queenslandica]
XP_019851834.1       -            405 PHD_4                PF16866.6     68    0.0038   17.7   4.5   1   3      0.34     1e+03    0.3   0.1     6    17   234   245   197   261 0.67 PREDICTED: zinc finger protein ubi-d4-like isoform X1 [Amphimedon queenslandica]
XP_019851834.1       -            405 PHD_4                PF16866.6     68    0.0038   17.7   4.5   2   3   1.3e-06    0.0038   17.7   4.5     6    67   303   358   298   362 0.79 PREDICTED: zinc finger protein ubi-d4-like isoform X1 [Amphimedon queenslandica]
XP_019851834.1       -            405 PHD_4                PF16866.6     68    0.0038   17.7   4.5   3   3      0.14   4.1e+02    1.6   6.3     7    67   361   405   359   405 0.76 PREDICTED: zinc finger protein ubi-d4-like isoform X1 [Amphimedon queenslandica]
XP_011404744.2       -            880 PHD_4                PF16866.6     68      0.21   12.1   1.8   1   2   8.3e-05      0.25   11.9   0.1    31    56   442   465   422   475 0.76 PREDICTED: ras guanine nucleotide exchange factor Q-like [Amphimedon queenslandica]
XP_011404744.2       -            880 PHD_4                PF16866.6     68      0.21   12.1   1.8   2   2       5.9   1.8e+04   -3.7   0.1    43    57   806   820   805   824 0.72 PREDICTED: ras guanine nucleotide exchange factor Q-like [Amphimedon queenslandica]
XP_019855288.1       -            447 PHD_4                PF16866.6     68      0.64   10.6   7.7   1   1   0.00064       1.9    9.0   7.7    24    67   411   446   394   447 0.84 PREDICTED: uncharacterized protein LOC109584123 [Amphimedon queenslandica]
XP_019861849.1       -            975 PHD_4                PF16866.6     68       2.1    8.9  12.6   1   2       2.2   6.7e+03   -2.3   0.0    26    43   106   123    99   130 0.68 PREDICTED: uncharacterized protein LOC109590368 [Amphimedon queenslandica]
XP_019861849.1       -            975 PHD_4                PF16866.6     68       2.1    8.9  12.6   2   2   0.00054       1.6    9.2   9.6    26    68   934   973   917   973 0.88 PREDICTED: uncharacterized protein LOC109590368 [Amphimedon queenslandica]
XP_019848920.1       -           1071 PHD_4                PF16866.6     68         3    8.4   8.9   1   2     0.038   1.1e+02    3.4   5.4    21    54   168   201   145   207 0.73 PREDICTED: diacylglycerol kinase zeta-like [Amphimedon queenslandica]
XP_019848920.1       -           1071 PHD_4                PF16866.6     68         3    8.4   8.9   2   2    0.0071        22    5.7   0.1    31    49   254   272   244   291 0.80 PREDICTED: diacylglycerol kinase zeta-like [Amphimedon queenslandica]
#
# Program:         hmmsearch
# Version:         3.3 (Nov 2019)
# Pipeline mode:   SEARCH
# Query file:      01_HMMs/07_PHD_4.hmm
# Target file:     02_Proteoms/20_AQUE_proteom.fa
# Option settings: hmmsearch --domtblout 03_Results_Hmmer/7_20.txt 01_HMMs/07_PHD_4.hmm 02_Proteoms/20_AQUE_proteom.fa 
# Current dir:     /home/paul/Documents
# Date:            Sun Jun 28 15:30:05 2020
# [ok]
