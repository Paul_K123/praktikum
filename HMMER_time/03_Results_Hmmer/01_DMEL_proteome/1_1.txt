#                                                                            --- full sequence --- -------------- this domain -------------   hmm coord   ali coord   env coord
# target name        accession   tlen query name           accession   qlen   E-value  score  bias   #  of  c-Evalue  i-Evalue  score  bias  from    to  from    to  from    to  acc description of target
#------------------- ---------- ----- -------------------- ---------- ----- --------- ------ ----- --- --- --------- --------- ------ ----- ----- ----- ----- ----- ----- ----- ---- ---------------------
NP_001262100.1       -            870 Amino_oxidase        PF01593.25   452  2.7e-136  456.2   0.0   1   1  2.3e-139  3.4e-136  455.9   0.0     1   451   254   804   254   805 0.95 suppressor of variegation 3-3, isoform C [Drosophila melanogaster]
NP_649194.1          -            890 Amino_oxidase        PF01593.25   452  2.8e-136  456.2   0.0   1   1  2.5e-139  3.7e-136  455.8   0.0     1   451   274   824   274   825 0.95 suppressor of variegation 3-3, isoform A [Drosophila melanogaster]
NP_730497.1          -            890 Amino_oxidase        PF01593.25   452  2.8e-136  456.2   0.0   1   1  2.5e-139  3.7e-136  455.8   0.0     1   451   274   824   274   825 0.95 suppressor of variegation 3-3, isoform B [Drosophila melanogaster]
NP_001261998.1       -            486 Amino_oxidase        PF01593.25   452   2.8e-93  314.4   0.0   1   1   2.2e-96   3.2e-93  314.3   0.0     2   451    26   475    25   476 0.92 uncharacterized protein Dmel_CG7460, isoform C [Drosophila melanogaster]
NP_649004.1          -            486 Amino_oxidase        PF01593.25   452   2.8e-93  314.4   0.0   1   1   2.2e-96   3.2e-93  314.3   0.0     2   451    26   475    25   476 0.92 uncharacterized protein Dmel_CG7460, isoform B [Drosophila melanogaster]
NP_001262380.1       -            583 Amino_oxidase        PF01593.25   452   2.9e-93  314.4   0.0   1   1   2.3e-96   3.4e-93  314.2   0.0     1   452    49   556    49   556 0.90 uncharacterized protein Dmel_CG8032, isoform B [Drosophila melanogaster]
NP_649811.1          -            583 Amino_oxidase        PF01593.25   452   2.9e-93  314.4   0.0   1   1   2.3e-96   3.4e-93  314.2   0.0     1   452    49   556    49   556 0.90 uncharacterized protein Dmel_CG8032, isoform A [Drosophila melanogaster]
NP_648269.1          -            476 Amino_oxidase        PF01593.25   452   1.8e-86  292.0   0.0   1   1   1.4e-89   2.1e-86  291.8   0.0     1   452    17   466    17   466 0.92 uncharacterized protein Dmel_CG5653 [Drosophila melanogaster]
NP_476608.1          -            504 Amino_oxidase        PF01593.25   452   2.5e-84  284.9   0.0   1   1     2e-87   2.9e-84  284.7   0.0     1   452    48   494    48   494 0.91 uncharacterized protein Dmel_CG10561 [Drosophila melanogaster]
NP_649005.1          -            479 Amino_oxidase        PF01593.25   452     4e-83  281.0   0.0   1   1   3.2e-86   4.6e-83  280.8   0.0     2   451    27   469    26   470 0.92 uncharacterized protein Dmel_CG6034 [Drosophila melanogaster]
NP_610641.1          -            509 Amino_oxidase        PF01593.25   452   4.2e-75  254.5   0.4   1   1   3.4e-78     5e-75  254.3   0.4     2   451    20   492    19   493 0.90 uncharacterized protein Dmel_CG7737, isoform A [Drosophila melanogaster]
NP_001286308.1       -            543 Amino_oxidase        PF01593.25   452   5.4e-75  254.2   0.4   1   1   4.4e-78   6.4e-75  253.9   0.4     2   451    54   526    53   527 0.90 uncharacterized protein Dmel_CG7737, isoform B [Drosophila melanogaster]
NP_651278.2          -            475 Amino_oxidase        PF01593.25   452   2.1e-53  183.0   0.0   1   1   1.6e-56   2.4e-53  182.8   0.0     1   451    10   472    10   473 0.91 protoporphyrinogen oxidase [Drosophila melanogaster]
NP_651239.1          -            477 Amino_oxidase        PF01593.25   452   7.9e-08   32.8   3.5   1   2      0.01        15    5.5   0.1     1    30    21    52    21    57 0.92 sheepish [Drosophila melanogaster]
NP_651239.1          -            477 Amino_oxidase        PF01593.25   452   7.9e-08   32.8   3.5   2   2   3.5e-08   5.2e-05   23.5   0.7   108   449    79   410    53   413 0.67 sheepish [Drosophila melanogaster]
NP_001246804.1       -           2115 Amino_oxidase        PF01593.25   452   1.8e-05   25.0   0.0   1   1   2.2e-08   3.3e-05   24.2   0.0     2    30  1778  1806  1777  1808 0.94 uncharacterized protein Dmel_CG9674, isoform F [Drosophila melanogaster]
NP_001261973.1       -           2115 Amino_oxidase        PF01593.25   452   1.8e-05   25.0   0.0   1   1   2.2e-08   3.3e-05   24.2   0.0     2    30  1778  1806  1777  1808 0.94 uncharacterized protein Dmel_CG9674, isoform G [Drosophila melanogaster]
NP_648922.1          -           2114 Amino_oxidase        PF01593.25   452   1.8e-05   25.0   0.0   1   1   2.2e-08   3.3e-05   24.2   0.0     2    30  1777  1805  1776  1807 0.94 uncharacterized protein Dmel_CG9674, isoform A [Drosophila melanogaster]
NP_788517.1          -           2114 Amino_oxidase        PF01593.25   452   1.8e-05   25.0   0.0   1   1   2.2e-08   3.3e-05   24.2   0.0     2    30  1777  1805  1776  1807 0.94 uncharacterized protein Dmel_CG9674, isoform D [Drosophila melanogaster]
NP_611859.1          -            416 Amino_oxidase        PF01593.25   452   0.00097   19.3   0.0   1   1   1.3e-06     0.002   18.3   0.0     2    28    12    38    11    49 0.94 Flavin-containing monooxygenase 1 [Drosophila melanogaster]
NP_001262000.1       -            504 Amino_oxidase        PF01593.25   452      0.26   11.3   0.0   1   1    0.0003      0.44   10.5   0.0   210   260   251   308   221   312 0.85 uncharacterized protein Dmel_CG7430, isoform B [Drosophila melanogaster]
NP_649017.1          -            504 Amino_oxidase        PF01593.25   452      0.26   11.3   0.0   1   1    0.0003      0.44   10.5   0.0   210   260   251   308   221   312 0.85 uncharacterized protein Dmel_CG7430, isoform A [Drosophila melanogaster]
#
# Program:         hmmsearch
# Version:         3.3 (Nov 2019)
# Pipeline mode:   SEARCH
# Query file:      01_HMMs/01_Amino_oxidase.hmm
# Target file:     02_Proteoms/01_DMEL_proteom.fa
# Option settings: hmmsearch --domtblout 03_Results_Hmmer/11.txt 01_HMMs/01_Amino_oxidase.hmm 02_Proteoms/01_DMEL_proteom.fa 
# Current dir:     /home/paul/Documents
# Date:            Thu Jun 25 18:11:25 2020
# [ok]
