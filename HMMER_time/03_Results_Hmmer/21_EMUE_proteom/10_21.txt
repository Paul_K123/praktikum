#                                                                            --- full sequence --- -------------- this domain -------------   hmm coord   ali coord   env coord
# target name        accession   tlen query name           accession   qlen   E-value  score  bias   #  of  c-Evalue  i-Evalue  score  bias  from    to  from    to  from    to  acc description of target
#------------------- ---------- ----- -------------------- ---------- ----- --------- ------ ----- --- --- --------- --------- ------ ----- ----- ----- ----- ----- ----- ----- ---- ---------------------
m.32357              -            434 zf-CW                PF07496.16    48     1e-14   54.9   6.8   1   2   2.8e-18     1e-14   54.9   6.8     1    48    59   105    59   105 0.92 g.32357  ORF g.32357 m.32357 type:5prime_partial len:434 (+) comp56171_c0_seq3:3-1304(+)
m.32357              -            434 zf-CW                PF07496.16    48     1e-14   54.9   6.8   2   2       1.4   5.2e+03   -1.8   0.1    27    34   328   335   315   356 0.51 g.32357  ORF g.32357 m.32357 type:5prime_partial len:434 (+) comp56171_c0_seq3:3-1304(+)
m.102189             -            380 zf-CW                PF07496.16    48     8e-14   52.0  10.6   1   1   6.2e-17   2.3e-13   50.6  10.6     2    47   128   171   127   172 0.95 g.102189  ORF g.102189 m.102189 type:5prime_partial len:380 (+) comp64986_c0_seq7:1-1140(+)
m.102193             -            418 zf-CW                PF07496.16    48   2.6e-13   50.4  10.6   1   2   7.1e-17   2.6e-13   50.4  10.6     2    47   128   171   127   172 0.95 g.102193  ORF g.102193 m.102193 type:5prime_partial len:418 (+) comp64986_c0_seq8:1-1254(+)
m.102193             -            418 zf-CW                PF07496.16    48   2.6e-13   50.4  10.6   2   2       1.2   4.2e+03   -1.5   0.1     3     9   338   344   337   346 0.85 g.102193  ORF g.102193 m.102193 type:5prime_partial len:418 (+) comp64986_c0_seq8:1-1254(+)
m.91424              -            817 zf-CW                PF07496.16    48   0.00035   21.2   5.4   1   2   5.6e-07     0.002   18.7   4.9     1    47   114   153   114   154 0.69 g.91424  ORF g.91424 m.91424 type:complete len:817 (+) comp64469_c0_seq3:318-2768(+)
m.91424              -            817 zf-CW                PF07496.16    48   0.00035   21.2   5.4   2   2       1.4   4.9e+03   -1.7   0.0    10    18   505   513   504   528 0.67 g.91424  ORF g.91424 m.91424 type:complete len:817 (+) comp64469_c0_seq3:318-2768(+)
m.91429              -            303 zf-CW                PF07496.16    48    0.0012   19.5   5.9   1   2         8   2.9e+04   -4.7   1.6    42    43    27    28    18    34 0.55 g.91429  ORF g.91429 m.91429 type:complete len:303 (+) comp64469_c0_seq4:318-1226(+)
m.91429              -            303 zf-CW                PF07496.16    48    0.0012   19.5   5.9   2   2   3.2e-07    0.0012   19.5   5.9     1    47   114   153   114   154 0.69 g.91429  ORF g.91429 m.91429 type:complete len:303 (+) comp64469_c0_seq4:318-1226(+)
m.105690             -            296 zf-CW                PF07496.16    48      0.03   15.0   0.9   1   2   8.1e-06      0.03   15.0   0.9     3    31   186   223   184   251 0.72 g.105690  ORF g.105690 m.105690 type:5prime_partial len:296 (+) comp65147_c0_seq3:3-890(+)
m.105690             -            296 zf-CW                PF07496.16    48      0.03   15.0   0.9   2   2       1.4     5e+03   -1.7   0.4     4     9   282   287   280   288 0.89 g.105690  ORF g.105690 m.105690 type:5prime_partial len:296 (+) comp65147_c0_seq3:3-890(+)
m.15238              -             87 zf-CW                PF07496.16    48      0.52   11.0   3.9   1   1   0.00038       1.4    9.7   3.9     3    31     5    42     3    71 0.71 g.15238  ORF g.15238 m.15238 type:internal len:87 (+) comp44575_c0_seq1:1-264(+)
m.215777             -            293 zf-CW                PF07496.16    48      0.71   10.6  11.8   1   2       2.9   1.1e+04   -2.8   0.0     8    17   148   157   146   163 0.76 g.215777  ORF g.215777 m.215777 type:5prime_partial len:293 (+) comp67870_c3_seq7:2-880(+)
m.215777             -            293 zf-CW                PF07496.16    48      0.71   10.6  11.8   2   2   0.00017       0.6   10.8   9.5     2    31   259   289   258   292 0.77 g.215777  ORF g.215777 m.215777 type:5prime_partial len:293 (+) comp67870_c3_seq7:2-880(+)
#
# Program:         hmmsearch
# Version:         3.3 (Nov 2019)
# Pipeline mode:   SEARCH
# Query file:      01_HMMs/10_zf-CW.hmm
# Target file:     02_Proteoms/21_EMUE_proteom.fa
# Option settings: hmmsearch --domtblout 03_Results_Hmmer/10_21.txt 01_HMMs/10_zf-CW.hmm 02_Proteoms/21_EMUE_proteom.fa 
# Current dir:     /home/paul/Documents
# Date:            Sun Jun 28 15:48:49 2020
# [ok]
