#                                                                            --- full sequence --- -------------- this domain -------------   hmm coord   ali coord   env coord
# target name        accession   tlen query name           accession   qlen   E-value  score  bias   #  of  c-Evalue  i-Evalue  score  bias  from    to  from    to  from    to  acc description of target
#------------------- ---------- ----- -------------------- ---------- ----- --------- ------ ----- --- --- --------- --------- ------ ----- ----- ----- ----- ----- ----- ----- ---- ---------------------
NP_723556.1          -            551 HARE-HTH             PF05066.14    72      0.26   12.4   0.0   1   1   8.7e-05      0.67   11.1   0.0    10    65   247   300   245   305 0.91 cylindromatosis, isoform B [Drosophila melanogaster]
NP_649855.3          -            274 HARE-HTH             PF05066.14    72      0.29   12.3   0.0   1   1   7.1e-05      0.55   11.4   0.0    38    66   131   157   119   162 0.83 uncharacterized protein Dmel_CG11977 [Drosophila melanogaster]
NP_609371.2          -            639 HARE-HTH             PF05066.14    72      0.32   12.1   0.0   1   1   0.00011      0.81   10.8   0.0    10    65   335   388   333   393 0.91 cylindromatosis, isoform E [Drosophila melanogaster]
NP_723554.1          -            639 HARE-HTH             PF05066.14    72      0.32   12.1   0.0   1   1   0.00011      0.81   10.8   0.0    10    65   335   388   333   393 0.91 cylindromatosis, isoform D [Drosophila melanogaster]
#
# Program:         hmmsearch
# Version:         3.3 (Nov 2019)
# Pipeline mode:   SEARCH
# Query file:      01_HMMs/05_HARE-HTH.hmm
# Target file:     02_Proteoms/01_DMEL_proteom.fa
# Option settings: hmmsearch --domtblout 03_Results_Hmmer/51.txt 01_HMMs/05_HARE-HTH.hmm 02_Proteoms/01_DMEL_proteom.fa 
# Current dir:     /home/paul/Documents
# Date:            Thu Jun 25 18:15:35 2020
# [ok]
