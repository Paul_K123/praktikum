#                                                                            --- full sequence --- -------------- this domain -------------   hmm coord   ali coord   env coord
# target name        accession   tlen query name           accession   qlen   E-value  score  bias   #  of  c-Evalue  i-Evalue  score  bias  from    to  from    to  from    to  acc description of target
#------------------- ---------- ----- -------------------- ---------- ----- --------- ------ ----- --- --- --------- --------- ------ ----- ----- ----- ----- ----- ----- ----- ---- ---------------------
NP_001261067.1       -           1043 zf-CW                PF07496.16    48       2.6    8.9  10.8   1   3     0.064   9.8e+02    0.6   3.0     2    11   527   536   526   537 0.90 polycomblike, isoform B [Drosophila melanogaster]
NP_001261067.1       -           1043 zf-CW                PF07496.16    48       2.6    8.9  10.8   2   3   2.1e-05      0.32   11.8   0.1     8    32   617   647   615   654 0.77 polycomblike, isoform B [Drosophila melanogaster]
NP_001261067.1       -           1043 zf-CW                PF07496.16    48       2.6    8.9  10.8   3   3      0.71   1.1e+04   -2.7   0.1    35    47   795   807   784   808 0.78 polycomblike, isoform B [Drosophila melanogaster]
NP_476672.1          -           1043 zf-CW                PF07496.16    48       2.6    8.9  10.8   1   3     0.064   9.8e+02    0.6   3.0     2    11   527   536   526   537 0.90 polycomblike, isoform A [Drosophila melanogaster]
NP_476672.1          -           1043 zf-CW                PF07496.16    48       2.6    8.9  10.8   2   3   2.1e-05      0.32   11.8   0.1     8    32   617   647   615   654 0.77 polycomblike, isoform A [Drosophila melanogaster]
NP_476672.1          -           1043 zf-CW                PF07496.16    48       2.6    8.9  10.8   3   3      0.71   1.1e+04   -2.7   0.1    35    47   795   807   784   808 0.78 polycomblike, isoform A [Drosophila melanogaster]
#
# Program:         hmmsearch
# Version:         3.3 (Nov 2019)
# Pipeline mode:   SEARCH
# Query file:      01_HMMs/10_zf-CW.hmm
# Target file:     02_Proteoms/01_DMEL_proteom.fa
# Option settings: hmmsearch --domtblout 03_Results_Hmmer/101.txt 01_HMMs/10_zf-CW.hmm 02_Proteoms/01_DMEL_proteom.fa 
# Current dir:     /home/paul/Documents
# Date:            Thu Jun 25 18:17:10 2020
# [ok]
