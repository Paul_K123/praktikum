#                                                                            --- full sequence --- -------------- this domain -------------   hmm coord   ali coord   env coord
# target name        accession   tlen query name           accession   qlen   E-value  score  bias   #  of  c-Evalue  i-Evalue  score  bias  from    to  from    to  from    to  acc description of target
#------------------- ---------- ----- -------------------- ---------- ----- --------- ------ ----- --- --- --------- --------- ------ ----- ----- ----- ----- ----- ----- ----- ---- ---------------------
XP_020623403.1       -            690 HARE-HTH             PF05066.14    72   9.5e-21   74.8   0.0   1   1   6.6e-25   2.1e-20   73.6   0.0     2    71    15    84    14    85 0.97 putative Polycomb group protein ASXL2 [Orbicella faveolata]
#
# Program:         hmmsearch
# Version:         3.3 (Nov 2019)
# Pipeline mode:   SEARCH
# Query file:      01_HMMs/05_HARE-HTH.hmm
# Target file:     02_Proteoms/12_OFAV_proteom.fa
# Option settings: hmmsearch --domtblout 03_Results_Hmmer/5_12.txt 01_HMMs/05_HARE-HTH.hmm 02_Proteoms/12_OFAV_proteom.fa 
# Current dir:     /home/paul/Documents
# Date:            Sun Jun 28 15:09:36 2020
# [ok]
