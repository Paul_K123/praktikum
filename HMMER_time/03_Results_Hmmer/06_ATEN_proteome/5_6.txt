#                                                                            --- full sequence --- -------------- this domain -------------   hmm coord   ali coord   env coord
# target name        accession   tlen query name           accession   qlen   E-value  score  bias   #  of  c-Evalue  i-Evalue  score  bias  from    to  from    to  from    to  acc description of target
#------------------- ---------- ----- -------------------- ---------- ----- --------- ------ ----- --- --- --------- --------- ------ ----- ----- ----- ----- ----- ----- ----- ---- ---------------------
XP_031553199.1       -            722 HARE-HTH             PF05066.14    72   9.9e-17   61.6   0.0   1   1   4.4e-20     2e-16   60.7   0.0     2    70    16    84    15    86 0.97 putative Polycomb group protein ASXL2 isoform X2 [Actinia tenebrosa]
XP_031553197.1       -            730 HARE-HTH             PF05066.14    72   6.6e-14   52.6   0.0   1   1   3.2e-17   1.4e-13   51.5   0.0     9    70    31    92    30    94 0.97 putative Polycomb group protein ASXL2 isoform X1 [Actinia tenebrosa]
XP_031565583.1       -            807 HARE-HTH             PF05066.14    72   1.8e-09   38.4   0.0   1   1   8.9e-13     4e-09   37.3   0.0     1    71    96   164    96   165 0.93 uncharacterized protein LOC116300783 isoform X2 [Actinia tenebrosa]
XP_031565582.1       -            825 HARE-HTH             PF05066.14    72   1.8e-09   38.4   0.0   1   1   9.2e-13   4.1e-09   37.2   0.0     1    71    96   164    96   165 0.93 uncharacterized protein LOC116300783 isoform X1 [Actinia tenebrosa]
XP_031558146.1       -            152 HARE-HTH             PF05066.14    72      0.13   13.2   0.0   1   1   5.3e-05      0.24   12.4   0.0    28    54   109   136    99   149 0.79 ubiquitin-conjugating enzyme E2 A [Actinia tenebrosa]
XP_031561529.1       -            181 HARE-HTH             PF05066.14    72      0.22   12.4   0.6   1   2       2.5   1.1e+04   -2.6   0.0    20    31    31    42    30    53 0.72 dynein regulatory complex protein 8-like [Actinia tenebrosa]
XP_031561529.1       -            181 HARE-HTH             PF05066.14    72      0.22   12.4   0.6   2   2   0.00011      0.48   11.4   0.1     7    34   119   147   116   161 0.80 dynein regulatory complex protein 8-like [Actinia tenebrosa]
#
# Program:         hmmsearch
# Version:         3.3 (Nov 2019)
# Pipeline mode:   SEARCH
# Query file:      01_HMMs/05_HARE-HTH.hmm
# Target file:     02_Proteoms/06_ATEN_proteom.fa
# Option settings: hmmsearch --domtblout 03_Results_Hmmer/5_6.txt 01_HMMs/05_HARE-HTH.hmm 02_Proteoms/06_ATEN_proteom.fa 
# Current dir:     /home/paul/Documents
# Date:            Fri Jun 26 13:41:34 2020
# [ok]
